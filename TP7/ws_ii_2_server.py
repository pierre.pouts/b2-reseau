import asyncio
import websockets
import redis.asyncio as redis

async def handle_client(websocket, path):
    client_address = websocket.remote_address
    print(f"Received connection from {client_address}")

    # Connexion au serveur Redis
    redis_client = redis.Redis(host="10.1.1.12", port=6379, decode_responses=True)

    try:
        pseudo_message = await websocket.recv()

        if pseudo_message.startswith("Hello|"):
            pseudo = pseudo_message.split("|")[1]

            # Vérifier si le pseudo est déjà pris
            if await redis_client.exists(pseudo):
                print(f"Pseudo '{pseudo}' is already taken. Closing connection.")
                return

            # Enregistrer le nouveau client dans Redis
            await redis_client.hset("clients", pseudo, "client")
            print(f"New client joined: {pseudo}")

            # Annoncer l'arrivée du nouveau client
            for existing_pseudo in await redis_client.hkeys("clients"):
                if existing_pseudo != pseudo:
                    announcement = f"Annonce: {pseudo} a rejoint la chatroom"
                    await websocket.send(announcement)

            # Récupérer et envoyer les derniers messages
            last_messages = await redis_client.lrange("chat_messages", -50, -1)
            for message in last_messages:
                await websocket.send(message)

            try:
                while True:
                    message = await websocket.recv()

                    # Ajouter le message à la liste des messages
                    message_str = f"{pseudo} a dit : {message}"
                    await redis_client.lpush("chat_messages", message_str)

                    # Diffuser le message à tous les clients
                    for existing_pseudo in await redis_client.hkeys("clients"):
                        if existing_pseudo != pseudo:
                            broadcast_message = f"\n{pseudo} a dit : {message}"
                            await redis_client.publish(existing_pseudo, broadcast_message)

            except websockets.exceptions.ConnectionClosedError:
                pass

    finally:
        print(f"Connection from {client_address} closed.")
        pseudo = await redis_client.hget("clients", pseudo)

        # Annoncer le départ du client
        for existing_pseudo in await redis_client.hkeys("clients"):
            if existing_pseudo != pseudo:
                announcement = f"Annonce: {pseudo} a quitté la chatroom"
                await redis_client.publish(existing_pseudo, announcement)

        # Supprimer les données du client de Redis
        await redis_client.hdel("clients", pseudo)

    # Fermer la connexion Redis
    await redis_client.aclose()

async def main():
    async with websockets.serve(
        handle_client, '10.1.1.10', 13337,
    ):
        print("Serving on ws://10.1.1.10:13337")

        await asyncio.Future()

if __name__ == "__main__":
    asyncio.run(main())
