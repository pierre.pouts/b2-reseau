

## I. Websockets

### 1. First steps


🌞 **`ws_i_1_server.py` et `ws_i_1_client.py`**

[Lien vers le code pour ws_i_1_server.py](./ws_i_1_server.py)

[Lien vers le code pour ws_i_1_client.py](./ws_i_1_client.py)

### 2. Client JS

🌞 **`ws_i_2_client.js`**

[Lien vers le code pour ws_i_2_client.js](./ws_i_2_client.js)

### 3. Chatroom magueule

🌞 **`ws_i_3_server.py` et `ws_i_3_client.{py,js}`**

[Lien vers le code pour ws_i_3_server.py](./ws_i_3_server.py)

[Lien vers le code pour ws_i_3_client.py](./ws_i_3_client..py)

[Lien vers le code pour ws_i_3_client.js](./ws_i_3_client.js)

## II. Base de données

### 1. Intro données
### 2. Redis

🌞 **`ws_ii_2_server.py`**

[Lien vers le code pour ws_ii_2_server.py](./ws_ii_2_server.py)

### 3. Bonus : MongoDB

🌞 **`ws_ii_3_server.py`**

[Lien vers le code pour ws_ii_3_server.py](./ws_ii_3_server.py)

