import socket
import sys

# Définition de l'adresse IP et du port du serveur
server_ip = '10.1.1.10'  # Remplace cela par l'adresse IP de ton serveur
server_port = 13337

# Création d'une socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connexion au serveur
    client_socket.connect((server_ip, server_port))
    print(f"Connecté avec succès au serveur {server_ip} sur le port {server_port}")

    # Saisie du message à envoyer au serveur
    message = input("Que veux-tu envoyer au serveur : ")
    client_socket.send(message.encode())

    # Réception de la réponse du serveur
    response = client_socket.recv(1024).decode()
    print(f"Réponse du serveur : {response}")

except Exception as e:
    print(f"Erreur : {e}")
    sys.exit(1)  # Quitte avec un code de retour indiquant une erreur

finally:
    # Fermeture propre de la connexion avec le serveur
    client_socket.close()
    sys.exit(0)
