import socket
import re

# Définition de l'adresse IP et du port du serveur
server_ip = '10.1.1.10'  # Remplace cela par l'adresse IP de ton serveur
server_port = 13337

# Création d'une socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connexion au serveur
    client_socket.connect((server_ip, server_port))
    print(f"Connecté au serveur {server_ip}:{server_port}")

    # Saisie du message à envoyer au serveur
    user_input = input("Saisis le message à envoyer au serveur: ")

    # Vérification que l'entrée est une string
    if not isinstance(user_input, str):
        raise TypeError("L'entrée doit être une chaîne de caractères.")

    # Vérification que la string contient soit "waf" soit "meo" à l'aide d'une expression régulière
    pattern = re.compile(r'(waf|meo)', re.IGNORECASE)
    if not pattern.search(user_input):
        raise ValueError("La chaîne doit contenir soit 'waf' soit 'meo'.")

    # Envoi du message au serveur
    client_socket.send(user_input.encode())

    # Réception de la réponse du serveur
    response = client_socket.recv(1024).decode()
    print(f"Réponse du serveur : {response}")

except TypeError as te:
    print(f"Erreur de type : {te}")
except ValueError as ve:
    print(f"Erreur de valeur : {ve}")
except Exception as e:
    print(f"Erreur inattendue : {e}")
finally:
    # Fermeture propre de la connexion avec le serveur
    client_socket.close()
