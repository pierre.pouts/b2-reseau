# I. Simple bs program

## 1. First steps

🌞 **`bs_server_I1.py`**

[Lien vers le code pour bs_server_I1.py](./bs_server.I1.py)

🌞 **`bs_client_I1.py`**

[Lien vers le code pour bs_client_I1.py](./bs_client_I1.py)

🌞 **`Commandes...`**

```
[pierre@bsserver tp4]$ sudo firewall-cmd --zone=public --add-port=13337/tcp --permanent
[sudo] password for pierre:
success
[pierre@bsserver tp4]$ sudo firewall-cmd --reload
success

[pierre@bsserver ~]$ sudo ss -tnlp | grep 13337
[sudo] password for pierre:
LISTEN 0      5          10.1.1.10:13337      0.0.0.0:*    users:(("python",pid=1410,fd=3))
```
```
[pierre@bsserver ~]$ sudo dnf install python3 -y
[sudo] password for pierre:
Last metadata expiration check: 0:14:11 ago on Tue 07 Nov 2023 02:16:31 PM CET.
Package python3-3.9.16-1.el9_2.2.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```
```
[pierre@bsserver tp4]$ python bs_server_I1.py
Le serveur écoute sur 10.1.1.10:13337
Connexion établie avec ('10.1.1.11', 40032)
Réponse du client : Meooooo !


[pierre@bsclient tp4]$ python bs_client_I1.py
Connecté au serveur 10.1.1.10:13337
Réponse du serveur : Hi mate !
```

## 2. User friendly

🌞 **`bs_client_I2.py`**

[Lien vers le code pour bs_client_I2.py](./bs_client_I2.py)

🌞 **`bs_server_I2.py`**

[Lien vers le code pour bs_serer_I2.py](./bs_server_I2.py)

## 3. You say client I hear control


🌞 **`bs_client_I3.py`**

[Lien vers le code pour bs_client_I3.py](./bs_client_I3.py)



# II. You say dev I say good practices

🌞 **`bs_server_II1.py`**

[Lien vers le code pour bs_client_II1.py](./bs_server_II1.py)

🌞 **`bs_server_II2A.py`**

```
[pierre@bsserver tp4]$ sudo mkdir -p /var/log/bs_server/
```

[Lien vers le code pour bs_server_II2A.py](./bs_server_II2A.py)

🌞 **`bs_client_II2B.py`**

```
[pierre@bsclient tp4]$ sudo mkdir -p /var/log/bs_client/
```

[Lien vers le code pour bs_client_II2B.py](./bs_client_II2B.py)



# III. COMPUTE

🌞 **`bs_client_III.py`**

[Lien vers le code pour bs_client_III.py](./bs_client_III.py)

🌞 **`bs_server_III.py`**

[Lien vers le code pour bs_server_III.py](./bs_server_III.py)




