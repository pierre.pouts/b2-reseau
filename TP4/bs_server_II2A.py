import socket
import argparse
import sys
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime, timedelta
import threading

def setup_logger():
    # Création du logger
    logger = logging.getLogger('bs_server')
    logger.setLevel(logging.DEBUG)

    # Création d'un formateur pour les logs
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # Création d'un gestionnaire de logs pour la console (niveau INFO)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    # Création d'un gestionnaire de logs pour le fichier (niveau DEBUG)
    file_handler = TimedRotatingFileHandler('/var/log/bs_server/bs_server.log', when='midnight', interval=1, backupCount=7)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

def check_last_client_time(logger):
    while True:
        # Vérification du temps sans client toutes les minutes
        if datetime.now() - last_client_time > timedelta(minutes=1):
            logger.warning("Aucun client depuis plus de une minute.")
        # Attendre 1 minute
        threading.Event().wait(60)

def start_server(port):
    # Création d'une socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    logger = setup_logger()

    global last_client_time
    last_client_time = datetime.now()

    # Démarrer le thread pour vérifier le temps sans client
    threading.Thread(target=check_last_client_time, args=(logger,), daemon=True).start()

    try:
        # Obtenir l'adresse IP de la machine
        ip_address = socket.gethostbyname(socket.gethostname())

        # Liaison de la socket à l'adresse IP et au port spécifiés
        server_socket.bind((ip_address, port))

        # Écoute de la socket
        server_socket.listen(5)
        logger.info(f"Lancement du serveur. Le serveur tourne sur {ip_address}:{port}")

        while True:
            # Attente de la connexion d'un client
            client_socket, client_address = server_socket.accept()
            logger.info(f"Connexion d'un client. Un client {client_address[0]} s'est connecté.")

            # Réception du message du client
            message = client_socket.recv(1024).decode()
            logger.info(f"Message reçu d'un client. Le client {client_address[0]} a envoyé : {message}.")

            # Traitement de la réponse en fonction du message du client
            if "meo" in message.lower():
                response = "Meo à toi confrère."
            elif "waf" in message.lower():
                response = "ptdr t ki"
            else:
                response = "Mes respects humble humain."

            # Envoi de la réponse au client
            client_socket.send(response.encode())
            logger.info(f"Message envoyé par le serveur. Réponse envoyée au client {client_address[0]} : {response}.")

            # Met à jour le temps du dernier client connecté
            last_client_time = datetime.now()

            # Fermeture de la connexion avec le client
            client_socket.close()

    except KeyboardInterrupt:
        pass
    finally:
        # Fermeture propre de la connexion avec le serveur
        server_socket.close()

def main():
    # Configuration de l'analyseur d'arguments
    parser = argparse.ArgumentParser(description='Serveur TCP avec gestion d\'options.')
    parser.add_argument('-p', '--port', type=int, default=13337, help='Numéro de port pour écouter les connexions. (Par défaut : 13337)')

    # Analyse des arguments de la ligne de commande
    args = parser.parse_args()

    # Traitement des options
    if args.port < 0 or args.port > 65535:
        print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
        exit(1)
    elif args.port <= 1024:
        print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au-dessus de 1024.")
        exit(2)

    start_server(args.port)

if __name__ == "__main__":
    main()
