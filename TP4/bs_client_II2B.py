import socket
import re
import logging
from logging.handlers import RotatingFileHandler

def setup_logger():
    logger = logging.getLogger('bs_client')
    logger.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(message)s')

    log_file_path = '/var/log/bs_client/bs_client.log'
    file_handler = RotatingFileHandler(log_file_path, maxBytes=10240, backupCount=1)
    file_handler.setLevel(logging.ERROR)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

try:
    # Configuration du logger
    logger = setup_logger()

    server_ip = '10.1.1.10'
    server_port = 13337

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        client_socket.connect((server_ip, server_port))
        print(f"Connecté au serveur {server_ip}:{server_port}")
        logger.info(f"Connexion réussie à {server_ip}:{server_port}.")
    except Exception as e:
        # Utilisation des codes ANSI pour afficher le texte en rouge dans la console
        error_message = f"\033[91mERROR Impossible de se connecter au serveur {server_ip} sur le port {server_port}.\033[0m"
        print(error_message)
        logger.error(error_message)

except Exception as e:
    print(f"Erreur inattendue : {e}")
finally:
    client_socket.close()
