import socket

# Définition de l'adresse IP et du port
ip_address = '10.1.1.10'  # Remplace cela par l'adresse IP de ton serveur
port = 13337

# Création d'une socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Liaison de la socket à l'adresse IP et au port spécifiés
server_socket.bind((ip_address, port))

# Écoute de la socket
server_socket.listen(5)
print(f"Le serveur écoute sur {ip_address}:{port}")

while True:
    # Attente de la connexion d'un client
    client_socket, client_address = server_socket.accept()
    print(f"Un client vient de se connecter et son IP c'est {client_address[0]}.")

    # Réception du message du client
    message = client_socket.recv(1024).decode()
    print(f"Message du client : {message}")

    # Traitement de la réponse en fonction du message du client
    if "meo" in message.lower():
        response = "Meo à toi confrère."
    elif "waf" in message.lower():
        response = "ptdr t ki"
    else:
        response = "Mes respects humble humain."

    # Envoi de la réponse au client
    client_socket.send(response.encode())

    # Fermeture de la connexion avec le client
    client_socket.close()
