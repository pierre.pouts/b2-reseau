# TP3 DEV : Python et réseau

# I. Ping

🌞 **`ping_simple.py`**

[Lien vers le code pour ping_simple](./ping_simple.py)

🌞 **`ping_arg.py`**

[Lien vers le code pour ping_arg](./ping_arg.py)

🌞 **`is_up.py`**

[Lien vers le code pour is_up](./is_up.py)

# II. DNS


🌞 **`lookup.py`**

[Lien vers le code pour lookup](./lookup.py)

# III. Get your IP

🌞 **`get_ip.py`**

[Lien vers le code pour get](./get_ip.py)

# IV. Mix

🌞 **`network.py`**

[Lien vers le code pour network](./network.py)

# V. Deploy


