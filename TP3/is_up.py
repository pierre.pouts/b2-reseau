import os
import sys

ping_command = f"ping {sys.argv[1]}"
ping_output = os.popen(ping_command).read()

if "octets=" in ping_output:
  print(f" Is up!")
else:
  print(f" Is down!")

# import os
# import subprocess
# from sys import argv

# ping_command = f"ping {argv[1]} > nul 2>&1"
# response = subprocess.call(ping_command, shell=True)

# if response == 0:
#   print(f" is up!")
# else:
#   print(f" is down!")




