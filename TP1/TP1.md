# TP1 : Maîtrise réseau du poste

☀️ **Carte réseau WiFi**

```
PS C:\Users\pierr> ipconfig /all

Carte réseau sans fil Wi-Fi :

Adresse physique . . . . . . . . . . . : A8-93-4A-71-BB-BB
Adresse IPv4. . . . . . . . . . . . . .: 10.33.76.219(préféré)
Masque de sous-réseau. . . . . . . . . : 255.255.240.0

```
☀️ **Déso pas déso**
```
10.33.64.0
10.33.79.255
4096

```


☀️ **Hostname**

```
PS C:\Users\pierr> hostname

LAPTOP-MDL3A6CL
```
☀️ **Passerelle du réseau**

```
PS C:\Users\pierr> ipconfig /all

   Passerelle par défaut. . . . . . . . . : fe80::1494:6cff:fe95:d364%17
                                       10.33.79.254
```


☀️ **Serveur DHCP et DNS**

```
PS C:\Users\pierr> ipconfig /all

 Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
 Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
```


☀️ **Table de routage**

```
PS C:\Users\pierr> route print -4

IPv4 Table de routage

Itinéraires actifs :
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.219     30
```


# II. Go further

> Toujours tout en ligne de commande.

---

☀️ **Hosts ?**
```
PS C:\Windows\System32\drivers\etc> cat .\hosts

1.1.1.1 B2.Hello.vous
```
```
PS C:\> ping B2.Hello.vous

Envoi d’une requête 'ping' sur B2.Hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=12 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=31 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=15 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=12 ms TTL=57
```

☀️ **Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...**
```
PS C:\Users\pierr> nslookup youtube.com
RéponseNom :    youtube.com
Addresses:  2a00:1450:4007:80c::200e
          172.217.20.174
          
PS C:\Users\pierr> netstat -n
TCP    10.33.76.186:54008     172.217.20.174:443     TIME_WAIT
```


☀️ **Requêtes DNS**
```
PS C:\Users\pierr> nslookup www.ynov.com

Réponse ne faisant pas autorité :
Nom :    www.ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          104.26.11.233
          172.67.74.226
          104.26.10.233

PS C:\Users\pierr> nslookup 174.43.238.89

Nom :    89.sub-174-43-238.myvzw.com
Address:  174.43.238.89
```



☀️ **Hop hop hop**

PS C:\Users\pierr> tracert www.ynov.com
```
PS C:\Users\pierr> tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [104.26.11.233]
avec un maximum de 30 sauts :

  1     5 ms     2 ms     2 ms  10.33.79.254
  2    22 ms     6 ms     4 ms  145.117.7.195.rev.sfr.net [195.7.117.145]
  3    30 ms     4 ms     4 ms  237.195.79.86.rev.sfr.net [86.79.195.237]
  4    29 ms     5 ms     5 ms  196.224.65.86.rev.sfr.net [86.65.224.196]
  5    42 ms    12 ms    14 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  6    33 ms    12 ms    12 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  7    46 ms    30 ms    21 ms  141.101.67.48
  8    11 ms    13 ms    12 ms  172.71.124.4
  9    30 ms    12 ms    15 ms  104.26.11.233
```


☀️ **IP publique**



☀️ **Scan réseau**
```
PS C:\Users\pierr> arp -a | select-string "10.33"

Interface : 10.33.76.219 --- 0x11
  10.33.70.131          30-89-4a-0e-25-22     dynamique
  10.33.71.81           a4-6b-b6-16-b5-02     dynamique
  10.33.71.87           c4-3d-1a-c1-f6-97     dynamique
  10.33.71.107          84-c5-a6-e4-58-1d     dynamique
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
```

# III. Le requin


☀️ **Capture ARP**

[Lien vers capture ARP](./paquet%20ARP.pcap)


☀️ **Capture DNS**

[Lien vers capture DNS](./DNS.pcap)

☀️ **Capture TCP**

[Lien vers capture DNS](./tcp.pcap)


