# TP5 : Coding Encoding Decoding

# I. Jouer avec l'encodage

# II. Opti calculatrice

## 0. Setup

## 1. Strings sur mesure

🌞 **`tp5_enc_client_1.py`**

[Lien vers le code pour tp5_enc_client_1.py](./tp5_enc_client_1.py)

🌞 **`tp5_enc_server_1.py`**

[Lien vers le code pour tp5_enc_server_1.py](./tp5_enc_server_1.py)


## 2. Code Encode Decode

🌞 **`tp5_enc_client_2.py`**

[Lien vers le code pour tp5_enc_client_2.py](./tp5_enc_client_2.py)


🌞 **`tp5_enc_server_2.py`**

[Lien vers le code pour tp5_enc_server_2.py](.tp5_enc_server_2.py)

# III. Serveur Web et HTTP

## 0. Ptite intro HTTP

## 1. Serveur Web

🌞 **`tp5_web_serv_1.py` un serveur HTTP** 

[Lien vers le code pour tp5_web_serv_1.py](./tp5_web_serv_1.py)

```
[pierre@bsclient py]$ curl 10.1.1.10:13337
<h1>Hello, je suis un serveur HTTP</h1>[pierre@bsclient py]
```

## 2. Client Web

🌞 **`tp5_web_client_2.py` un client HTTP** super basique

[Lien vers le code pour tp5_web_client_2.py](./tp5_web_client_2.py)

```
[pierre@bsclient py]$ python tp5_web_client_2.py
Status: 200 OK
Headers: [('Server', 'SimpleHTTP/0.6 Python/3.9.16'), ('Date', 'Fri, 01 Dec 2023 16:41:01 GMT'), ('Content-type', 'text/html')]
Response body: <h1>Hello, je suis un serveur HTTP</h1>
```
## 3. Délivrer des pages web

🌞 **`tp5_web_serv_3.py`**

```
[pierre@bsserver server_web]$ python tp5_web_serv_3.py
Serveur HTTP démarré...
10.1.1.11 - - [01/Dec/2023 18:14:45] "GET /toto.html HTTP/1.1" 200 -
```
```
[pierre@bsclient py]$ python tp5_web_client_2.py
Status: 200 OK
Headers: [('Server', 'SimpleHTTP/0.6 Python/3.9.16'), ('Date', 'Fri, 01 Dec 2023 17:14:45 GMT'), ('Content-type', 'type/html')]
Response body: <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ma Page Web</title>
</head>
<body>

    <h1>Bienvenue sur ma page web !</h1>
    <p>C'est une page HTML de base.</p>

</body>
</html>
```


[Lien vers le code pour tp5_web_serv_3.py](./tp5_web_serv_3.py)

## 4. Quelques logs

🌞 **`tp5_web_serv_4.py`**

[Lien vers le code pour tp5_web_serv_4.py](./tp5_web_serv_4.py)

## 5. File download

🌞 **`tp5_web_serv_5.py`**

[Lien vers le code pour tp5_web_serv_5.py](./tp5_web_serv_5.py)
