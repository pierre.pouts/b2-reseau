import http.client

def simple_get_request():
    conn = http.client.HTTPConnection('10.1.1.10', 13337)
    conn.request("GET", "/")
    
    response = conn.getresponse()

    print(f"Status: {response.status} {response.reason}")
    print("Headers:", response.getheaders())

    data = response.read()
    print("Response body:", data.decode('utf-8'))

    conn.close()

if __name__ == "__main__":
    simple_get_request()
