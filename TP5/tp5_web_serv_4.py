import http.server
import socketserver
import logging

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        file_path = self.translate_path(self.path)

        try:
            with open(file_path, "rb") as file:
                file_content = file.read()

                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(file_content)

                
                logging.info(f"Requête du client: {self.client_address[0]} - Fichier demandé: {self.path}")
        except FileNotFoundError:
            self.send_error(404, "File Not Found")

           
            logging.error(f"Requête du client: {self.client_address[0]} - Fichier non trouvé: {self.path}")

if __name__ == "__main__":
    
    log_file_path = '/var/log/bs_server/server.log'
    logging.basicConfig(filename=log_file_path, level=logging.INFO)

    with socketserver.TCPServer(('10.1.1.10', 13337), MyHandler) as httpd:
        print('\033[94mServeur HTTP démarré...\033[0m')
        httpd.serve_forever()
