import asyncio

async def handle_client(reader, writer):
    # Get client information
    client_address = writer.get_extra_info('peername')
    print(f"Received connection from {client_address}")

    while True:
        data = await reader.read(1024)
        if not data:
            break
        message = data.decode()
        print(f"Message received from {client_address[0]}:{client_address[1]}: {message}")

    print(f"Connection from {client_address} closed.")
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '10.1.1.10', 13337)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
