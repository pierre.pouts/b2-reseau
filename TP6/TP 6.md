# TP6 : Chat room

## I. Débuts avec l'asynchrone

# I. Faire joujou avec l'asynchrone

## 1. Premiers pas

🌞 **`sleep_and_print.py`**

[Lien vers le code pour sleep_and_print.py](./sleep_and_print.py)


🌞 **`sleep_and_print_async.py`**

[Lien vers le code pour sleep_and_print_async.py](./sleep_and_print_async.py)


## II. Chat Room

# II. Chat room

## 1. Intro

## 2. Première version


🌞 `chat_server_ii_2.py`

[Lien vers le code pour chat_server_ii_2.py](./chat_server_ii_2.py)

🌞 `chat_client_ii_2.py`

[Lien vers le code pour chat_client_ii_2.py](./chat_client_ii_2.py)

## 3. Client asynchrone

🌞 `chat_client_ii_3.py`

[Lien vers le code pour chat_client_ii_3.py](./chat_client_ii_3.py)


🌞 `chat_server_ii_3.py`

[Lien vers le code pour chat_server_ii_3.py](./chat_server_ii_3.py)


## 4. Un chat fonctionnel


🌞 `chat_server_ii_4.py`

[Lien vers le code pour chat_server_ii_4.py](./chat_server_ii_4.py)


## 5. Gérer des pseudos


🌞 `chat_client_ii_5.py`

[Lien vers le code pour chat_client_ii_5.py](./chat_client_ii_5.py)

🌞 `chat_server_ii_5.py`

[Lien vers le code pour chat_server_ii_5.py](./chat_server_ii_5.py)



## 6. Déconnexion


🌞 `chat_server_ii_6.py` et `chat_client_ii_6.py`

[Lien vers le code pour chat_server_ii_6.py](./chat_server_ii_6.py)

[Lien vers le code pour chat_client_ii_6.py](./chat_client_ii_6.py)

## III. Bonus
# III. Chat Room bonus

## 1. Basic Cosmetic



## 2. Gestion d'ID

## 2. Logs

## 3. Config et arguments


## 4. Encodage maison

## 5. Envoi d'image

## 6. Gestion d'historique

## 7. Plusieurs room
